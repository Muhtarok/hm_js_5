"use strict"

// Теоретичні питання
// 1. Як можна сторити функцію та як ми можемо її викликати? 
// 2. Що таке оператор return в JavaScript? Як його використовувати в функціях?
// 3. Що таке параметри та аргументи в функіях та в яких випадках вони використовуються?
// 4. Як передати функцію аргументом в іншу функцію? 

// Практичні завдання
// 1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.

console.log("1. function Declaration, Function experession, стрілкова функція");
console.log("2. Return повертає визначені дані після того, як функція виконала свою роботу, наприклад, ми створили функцію з трьома параметрами(змінними), а return поверне результат їх множення");
console.log("3. Параметр – це змінна між дужками функції (використовується під час оголошення функції) Аргумент – це значення, передане в функцію під час її виклику (використовується під час виконання функції)");
console.log("4. execFunc(sayHello); sayHello є функцією, оголошеною раніше. наприклад,з привітанням, таким чином execFunc виконає другу фукнцію sayHello")

console.log("PRACTICE TASKS");

console.log("TASK 1");

const getValidatedNumber = (message = "Enter a number") => {
    let userNumber = prompt(message);
    while (isNaN(userNumber) || userNumber === null || userNumber === " " || userNumber === "") {
        userNumber = prompt(message);
    }
    return +userNumber;
}



const divisionFunctionTry = (firstNumber, secondNumber) => {
    firstNumber = getValidatedNumber("Enter Number 1");
    secondNumber = getValidatedNumber("Enter Number 2");
    return firstNumber / secondNumber;
}

console.log(divisionFunctionTry());



"use strict"

// 2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.

// Технічні вимоги:
// - Отримати за допомогою модального вікна браузера два числа. Провалідувати отримані значення(перевірити, що отримано числа). Якщо користувач ввів не число, запитувати до тих пір, поки не введе число
// - Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /. Провалідувати отримане значення. Якщо користувач ввів не передбачене значення, вивести alert('Такої операції не існує').
// - Створити функцію, в яку передати два значення та операцію.
// - Вивести у консоль результат виконання функції.

console.log("TASK 2");

const getValidatedNumber = (message = "Enter a number") => {
    let userNumber = prompt(message);
    while (isNaN(userNumber) || userNumber === null || userNumber === " " || userNumber === "") {
        alert("Try again");
        userNumber = prompt(message);
    }
    return +userNumber;
}


const getFirstNumber = () => {
    let numberFirst = getValidatedNumber("Enter Number 1");
    while (numberFirst === null || numberFirst === ""  ||  numberFirst === " ") {
        numberFirst = getValidatedNumber("Enter Number 1");
    }
    return numberFirst;
}

const getSecondNumber = () => {
    let secondNumber = getValidatedNumber("Enter Number 2");
    while (secondNumber === null || secondNumber === "" || secondNumber === " ") {
        secondNumber = getValidatedNumber("Enter Number 2");
    }
    return secondNumber;
}

const getValidatedMathSign = (message = "Enter a sign") => {
    let userMathSign = prompt(message);
    while (userMathSign !== "+" && userMathSign !== "*" && userMathSign !== "-" && userMathSign !== "/") {
        alert('Такої операції не існує');
        userMathSign = prompt(message);
    }
    return userMathSign;
}


const calcTwoNumbers = (result) => {
    const firstNumber1 = getFirstNumber();
    const secondNumber = getSecondNumber();
    const mathSign = getValidatedMathSign();

    if (mathSign === "+") {
        result = firstNumber1 + secondNumber;
    } else if (mathSign === "*") {
        result = firstNumber1 * secondNumber;
    } else if (mathSign === "-") {
        result = firstNumber1 - secondNumber;
    } else if (mathSign === "/") {
        result = firstNumber1 / secondNumber;
    }
    return result;
}

console.log(calcTwoNumbers());





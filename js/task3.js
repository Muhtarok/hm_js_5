"use strict"

// 3. Опціонально. Завдання: 
// Реалізувати функцію підрахунку факторіалу числа. 
// Технічні вимоги:
// - Отримати за допомогою модального вікна браузера число, яке введе користувач.
// - За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.
// - Використовувати синтаксис ES6 для роботи зі змінними та функціями.

console.log("TASK 3");



const calcFactorial = (n) => {

    let result;
    if (n === 1) return 1;
    else {
        result = n * calcFactorial(n - 1);
        return result;
    }
}

let userNumber = +prompt("Enter a number");
console.log(calcFactorial(userNumber));

// function calcFactorial(n) {
//     if (n == 1) return 1;
//     else return n * calcFactorial(n - 1);
// }
// var userNum = +prompt('Enter a number');
// alert(calcFactorial(userNum));